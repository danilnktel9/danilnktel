const path = require('path');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PATHS = {
	src: path.join(__dirname, 'src'),
	build: path.join(__dirname, 'build'),
	public: path.join(__dirname, 'public')
};

const common = {
  entry: PATHS.src + '/index.js',
  output: {
		path: PATHS.build,
    filename: 'js/main.js'
	},

  module: {
    rules: [
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
				options: {
					name: 'img/[name].[ext]'
				}
			},
			{
				test: /\.svg$/,
				loader: 'url-loader'
			},
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.sass$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!sass-loader'
        })
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json', '*']
  },

  plugins: [
		new ExtractTextPlugin('./css/main.css'),
		new HtmlWebpackPlugin({
			minify: {
				collapseWhitespace: true
			},
			hash: true,
			template: PATHS.public + '/index.html'
		})
  ]
}

const developConfig = {
	devServer: {
		stats: 'errors-only',
		port: 3080
	},
	devtool: 'inline-source-map',
	mode: 'development'
};

module.exports = env => env === 'production' ? common : merge([common, developConfig]);