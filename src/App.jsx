import React, { Component, Fragment } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import './style/default.sass';

import Header from './containers/HeaderContainer';
import ResetPassword from './containers/ResetPasswordContainer';
import MainPage from './components/MainPage';
import Footer from './components/Footer';

export default class App extends Component {
	// componentDidMount() {
	// 	if(document.cookie.match(/(userId=)(\S+)/) !== null) {
	// 		this.props.userInfo(document.cookie.match(/(userId=)(\S+)/)[2]);
	// 	} else this.props.logout();
	// }

  render() {
    return (
      <Fragment>
        <Header />
				<Switch>
					<Redirect from="/_=_/" to="/" />
				</Switch>
				<Route exact path="/" component={MainPage}/>
				<Route exact path="/session/reset-password" component={ResetPassword}/>
				<Footer />
      </Fragment>
    )
  }
}