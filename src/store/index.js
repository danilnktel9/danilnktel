import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { autoRehydrate, persistStore } from 'redux-persist';

import reducers from './reducers';

const middleware = [thunk];

// TODO(seiv): uncomment for development
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development') {
  middleware.push(logger);
}

const store = createStore(
  reducers,
  applyMiddleware(...middleware),
  autoRehydrate()
);

persistStore(store, {blacklist: ['registrationForm', 'headerPopupShow']});

export default store;