import options from '../../../utils/fetchOptions';

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const FORM_VERIFY = 'FORM_VERIFY';
export const HEADER_POPUP_TOGGLE = 'HEADER_POPUP_TOGGLE';
export const RESET_FORM_VALIDATION = 'RESET_FORM_VALIDATION';

const fetchReq = (path, body) => fetch(`https://localhost:8080/${path}`, {...options, body: JSON.stringify(body)}).then(res => res.json());

export const signUp = (body, checkbox) => (dispatch, store) => {
	const state = store();
	let error = 1;
	for(let label in state.registrationForm) {
		if(state.registrationForm[label].errorMessage) error = 0;
	}
	if(error && checkbox) {
		fetchReq('newUser', body)
			.then(res => {
				if(res.success) {
					dispatch({
						type: LOGIN,
						payload: {
							user: {
								name: `${body.first_name} ${body.last_name}`
							}
						}
					})
				} else {
					console.log(res.errorCode);
				}
			})
	}
}

export const signIn = body => (dispatch, store) => {
	const state = store();
	if(!state.registrationForm.email.errorMessage) {
		fetchReq('login', body)
		.then(res => {
			if(res.success) {
				dispatch({
					type: LOGIN,
					payload: {
						user: {
							name: res.payload
						}
					}
				})
			} else {
				console.log(res.errorCode);
			}
		})
	}
}

export const logout = () => dispatch => {
	fetchReq('logout', {})
		.then(res => {
			if(res.success) {
				dispatch({
					type: LOGOUT
				})
			} else {
				console.log(res.errorCode);
			}
		})
}

export const resetFormValidation = () => dispatch => {
	dispatch({
		type: RESET_FORM_VALIDATION
	})
} 

export const emailValidation = email => dispatch => {
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(re.test(String(email).toLowerCase())) {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'email',
				message: null
			}
		})
	} else {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'email',
				message: 'Email is not correct'
			}
		})
	}
}

export const firstNameValidation = first_name => dispatch => {
	if(first_name.length >= 4 && first_name.length <= 30) {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'first_name',
				message: null
			}
		})
	} else {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'first_name',
				message: 'Name is not correct'
			}
		})
	}
}

export const lastNameValidation = last_name => dispatch => {
	if(last_name.length >= 4 && last_name.length <= 30) {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'last_name',
				message: null
			}
		})
	} else {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'last_name',
				message: 'Lastname is not correct'
			}
		})
	}
}

export const passwordValidation = (password, confirm_password) => dispatch => {
	if(password.length >= 4 && password.length <= 30) {
		confirmPasswordValidation(password, confirm_password)(dispatch);
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'password',
				message: null
			}
		})
	} else {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'password',
				message: 'Password is not correct'
			}
		})
	}
}

export const confirmPasswordValidation = (password, confirm_password) => dispatch => {
	if(password === confirm_password) {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'confirm_password',
				message: null
			}
		})
	} else {
		dispatch({
			type: FORM_VERIFY,
			payload: {
				label: 'confirm_password',
				message: 'Confirm password is not correct'
			}
		})
	}
}

export const headerPopupToggle = sign => dispatch => {
	dispatch({
		type: HEADER_POPUP_TOGGLE,
		payload: {
			sign
		}
	})
}

export const userInfo = userId => dispatch => {
	fetchReq('user-info', { userId })
		.then(res => {
			if(res.success) {
				dispatch({
					type: LOGIN,
					payload: {
						user: {
							name: res.payload
						}
					}
				})
			} else {
				dispatch({
					type: LOGOUT
				})
			}
		})
}

export const resetPassword = email => dispatch => {
	fetchReq('recover-password', { email })
		.then(res => {
			window.location.href = '/';
		})
}