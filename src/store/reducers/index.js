import initState from '../initState';
import { LOGIN, LOGOUT, FORM_VERIFY, HEADER_POPUP_TOGGLE, RESET_FORM_VALIDATION } from '../actions';

export default (state = initState, action) => {
  switch (action.type) {
		case HEADER_POPUP_TOGGLE:
			return {...state, headerPopupShow: action.payload.sign};

		case LOGIN:
			return {...state, user: action.payload.user};
		
		case LOGOUT:
			return {...state, user: null, headerPopupShow: false};
		
		case FORM_VERIFY:
			const registrationForm = {...state.registrationForm};
			registrationForm[action.payload.label].errorMessage = action.payload.message;
			return {...state, registrationForm};
		
		case RESET_FORM_VALIDATION:
			const form = {...state.registrationForm};
			for(let label in form) form[label].errorMessage = null;
			return {...state, registrationForm: form};

		case 'persist/REHYDRATE':
			return {...state, persistedState: action.payload}

    default:
      return state;
  }
};
