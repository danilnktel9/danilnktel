export default {
	headerPopupShow: false,
	user: null,
	registrationForm: {
		email: {
			errorMessage: null
		},
		first_name: {
			errorMessage: null
		},
		last_name: {
			errorMessage: null
		},
		password: {
			errorMessage: null
		},
		confirm_password: {
			errorMessage: null
		}
	}
}