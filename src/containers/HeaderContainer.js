import { connect } from 'react-redux';
import Header from '../components/Header';

import { headerPopupToggle } from '../store/actions';

const mstp = state => ({
	user: state.user,
	popupShow: state.headerPopupShow
})

const mdtp = {
	headerPopupToggle
}

export default connect(mstp, mdtp)(Header);