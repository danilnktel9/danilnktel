import { connect } from 'react-redux';
import ResetPassword from '../components/ResetPassword';

import { resetPassword } from '../store/actions';

const mdtp = {
	resetPassword
}

export default connect(null, mdtp)(ResetPassword);