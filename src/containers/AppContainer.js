import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import App from '../App';

import { logout, userInfo } from '../store/actions';

const mdtp = {
	logout,
	userInfo
}

export default withRouter(connect(null, mdtp)(App));