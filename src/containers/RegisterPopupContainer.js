import { connect } from 'react-redux';
import RegisterPopup from '../components/Header/RegisterPopup';

import { signUp, signIn, emailValidation, firstNameValidation, lastNameValidation, passwordValidation, confirmPasswordValidation, headerPopupToggle, resetFormValidation } from '../store/actions';

const mstp = state => ({
	registrationForm: state.registrationForm
})

const mdtp = {
	signUp,
	signIn,
	emailValidation,
	firstNameValidation,
	lastNameValidation,
	passwordValidation,
	confirmPasswordValidation,
	headerPopupToggle,
	resetFormValidation
};

export default connect(mstp, mdtp)(RegisterPopup);