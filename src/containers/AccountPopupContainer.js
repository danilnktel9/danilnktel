import { connect } from 'react-redux';
import AccountPopup from '../components/Header/AccountPopup';

import { logout } from '../store/actions';

const mstp = state => ({
	user: state.user
})

const mdtp = {
	logout
}

export default connect(mstp, mdtp)(AccountPopup);