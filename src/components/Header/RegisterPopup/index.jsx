import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';

import './style.sass';

export default class RegisterPopup extends Component {
	constructor() {
		super();

		this.state = {
			tabIndex: 0
		};
		
		this.email = React.createRef();
		this.first_name = React.createRef();
		this.last_name = React.createRef();
		this.password = React.createRef();
		this.confirm_password = React.createRef();
		this.checkbox = React.createRef();
	}

	componentWillUnmount() {
		this.props.resetFormValidation();
	}

	val = input => input.current.value;

	changeTabIndex(tabIndex) {
		this.setState(state => {
			if(state.tabIndex == tabIndex) return null;
			else {
				this.props.resetFormValidation();
				return { tabIndex };
			}
		});
	}

	userRegistration(event) {
		event.preventDefault();

		const email = this.val(this.email);
		const first_name = this.val(this.first_name);
		const last_name = this.val(this.last_name);
		const password = this.val(this.password);
		const checkbox = this.checkbox.current.checked;

		this.props.signUp({email, first_name, last_name, password}, checkbox);
	}

	userLogin(event) {
		event.preventDefault();

		const email = this.val(this.email);
		const password = this.val(this.password);

		this.props.signIn({email, password});
	}

	signIn() {
		const { email } = this.props.registrationForm;
		return (
			<Fragment>
				<div id="login-form">
					<form onSubmit={ev => this.userLogin(ev)}>
						<label>
							<span>Логин (email)</span>
							<input 
								className={Boolean(email.errorMessage) ? 'error' : ''}
								onBlur={() => this.props.emailValidation(this.email.current.value)} 
								ref={this.email} name="email" type="text" required/>
						</label>
						<label>
							<span>Пароль</span>
							<input ref={this.password} type="password"/>
						</label>
						<button>Войти</button>
					</form>
				</div>
				<div className="reset-password">
					<Link onClick={() => this.props.headerPopupToggle(false)} to="/session/reset-password">Я забыл пароль</Link>
				</div>
				<div className="social">
					<span>Быстрый вход через</span>
					<ul>
						<li className="facebook">
							<a href="/login/facebook"><i className="fa fa-facebook-square"></i></a>
						</li>
						<li className="twitter">
							<a href="/login/twitter"><i className="fa fa-twitter-square"></i></a>
						</li>
						<li className="google">
							<a href="/login/google"><i className="fa fa-google-plus-square"></i></a>
						</li>
						<li className="linkedin">
							<a href="/login/linkedin"><i className="fa fa-linkedin-square"></i></a>
						</li>
					</ul>
				</div>
			</Fragment>
		)
	}

	signUp() {
		const { email, first_name, last_name, password, confirm_password } = this.props.registrationForm;
		return (
			<Fragment>
				<form onSubmit={ev => this.userRegistration(ev)} className="signup">
					<label>
						<span>Логин (email)</span>
						<input 
							className={Boolean(email.errorMessage) ? 'error' : ''}
							onBlur={() => this.props.emailValidation(this.email.current.value)} 
							ref={this.email} name="email" type="text" required/>
					</label>
					<label>
						<span>Имя</span>
						<input 
							className={Boolean(first_name.errorMessage) ? 'error' : ''}
							onBlur={() => this.props.firstNameValidation(this.first_name.current.value)} 
							ref={this.first_name} name="name" type="text" required/>
					</label>
					<label>
						<span>Фамилия</span>
						<input 
							className={Boolean(last_name.errorMessage) ? 'error' : ''}
							onBlur={() => this.props.lastNameValidation(this.last_name.current.value)} 
							ref={this.last_name} type="text" required/>
					</label>
					<label>
						<span>Пароль</span>
						<input 
							className={Boolean(password.errorMessage) ? 'error' : ''}
							onBlur={() => this.props.passwordValidation(this.password.current.value, this.confirm_password.current.value)} 
							ref={this.password} type="password" required/>
					</label>
					<label>
						<span>Подтверждение пароля</span>
						<input 
							className={Boolean(confirm_password.errorMessage) ? 'error' : ''}
							onBlur={() => this.props.confirmPasswordValidation(this.password.current.value, this.confirm_password.current.value)} 
							ref={this.confirm_password} type="password" required/>
					</label>
					<button>Регистрация</button>
				</form>
				<div className="rules">
					<input ref={this.checkbox} type="checkbox" />
					<span>Я согласен с <a href="#">Правилами</a></span>
				</div>
			</Fragment>
		)
	}

  render() {
    return (
      <div className="register-popup">
				<div onClick={this.props.popupToggle} className="account-button-popup">
					<i aria-hidden="true" className="fa fa-user margin-right"></i>
					<span>Вход / Регистрация</span>
					<i aria-hidden="true" className="fa fa-caret-up"></i>
				</div>
				<div className="dropdown-content">
					<div className="tabs">
						<span onClick={() => this.changeTabIndex(0)} className={this.state.tabIndex ? '' : 'active'}>Войти</span>
						<span onClick={() => this.changeTabIndex(1)} className={this.state.tabIndex ? 'active' : ''}>Регистрация</span>
					</div>
					
					{ this.state.tabIndex ? this.signUp() : this.signIn() }
				</div>
			</div>
    )
  }
}