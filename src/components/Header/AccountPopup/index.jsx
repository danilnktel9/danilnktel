import React, { Component, Fragment } from 'react';

import './style.sass';

export default class AccountPopup extends Component {
  render() {
    return (
      <div className="account-popup">
				<div onClick={this.props.popupToggle} className="account-button-popup">
					<i aria-hidden="true" className="fa fa-user margin-right"></i>
					<span>{this.props.user.name}</span>
					<i aria-hidden="true" className="fa fa-caret-up"></i>
				</div>
				<nav>
					<ul>
						<li>Профиль</li>
						<li>Специалисты</li>
						<li>Настройки</li>
						<li onClick={this.props.logout}>
							<i aria-hidden="true" className="fa fa-sign-out fa-bigger text-gray"></i>
							<span>Выйти</span>
						</li>
					</ul>
				</nav>
			</div>
    )
  }
}