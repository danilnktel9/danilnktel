import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../../resources/svg/logo.svg';

import './style.sass';
import RegisterPopup from '../../containers/RegisterPopupContainer';
import AccountPopup from '../../containers/AccountPopupContainer';

export default class Header extends Component {
	popupToggle(sign) {
		this.props.headerPopupToggle(sign);
	}

  render() {
    return (
      <header>
        <div onClick={() => this.popupToggle(false)} className="logo">
          <Link to="/">
            <img src={Logo} alt="Logo"/>
          </Link>
        </div>
				<div className="button-wrap">
					<div className="account-button">
						<div onClick={() => this.popupToggle(true)} className="text">
							<i aria-hidden="true" className="fa fa-user margin-right"></i>
							{
								this.props.user ? 
									<span>{this.props.user.name}</span>
										:
											<span>Вход / Регистрация</span>
							}
							<i aria-hidden="true" className="fa fa-caret-down"></i>
						</div>

						{
							this.props.user && this.props.popupShow ? 
								<AccountPopup popupToggle={() => this.popupToggle(false)} /> : 
									this.props.popupShow && 
										<RegisterPopup popupToggle={() => this.popupToggle(false)} />
						}
					</div>
					<div className="language-button">
						<div className="text">
							<span>Русский</span>
							<i aria-hidden="true" className="fa fa-caret-down"></i>
						</div>
					</div>
				</div>
      </header>
    )
  }
}