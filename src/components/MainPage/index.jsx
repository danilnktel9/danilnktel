import React, { Component, Fragment } from 'react';

import Carousel from './Carousel';
import ServiceList from './ServiceList';
import Credo from './Credo';

export default class MainPage extends Component {
  render() {
    return (
      <Fragment>
				<Carousel />
				<ServiceList />
				<Credo />
      </Fragment>
    )
  }
}