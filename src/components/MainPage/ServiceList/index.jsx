import React, { Component } from 'react';
import list from '../../../constants/ServiceList';
import lozalText from '../../../localization/ServiceList';

import './style.sass';

export default class ServiceList extends Component {
	text = lozalText['ru'];
  render() {
    return (
      <section id="service-list">
				{
					list.map((item, i) => (
						<div key={i} className="item">
							<div className="info">
								<div className="title">
									<span>{this.text[item.title].title}</span>
								</div>
								<div className="line"></div>
								<div className="text">
									<p>{this.text[item.title].text}</p>
								</div>
							</div>
							<div className="icon">
								<img src={item.data} alt="Alt"/>
							</div>
						</div>
					))
				}
			</section>
    )
  }
}