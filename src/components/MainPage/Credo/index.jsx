import React, { Component } from 'react';

import './style.sass';

export default class Header extends Component {
  render() {
    return (
      <section id="credo">
				<span>EAZZYCUT</span>
				<span>EASY STYLING PERFECTION</span>
				<span>AT ITS FINEST</span>
			</section>
    )
  }
}