import React, { Component } from 'react';
import Cats from '../../../../resources/svg/cats-background.svg';

import './style.sass';

export default class Carousel extends Component {
  render() {
    return (
      <section id="carousel">
				<div className="svg-cat">
					<img src={Cats} alt="Cats"/>
				</div>
				<div className="help-text">
					<span>Нужна ретушь фото? Значит Вам к нам!</span>
				</div>
				<div className="button">
					<span>Хочу классно!</span>
				</div>
			</section>
    )
  }
}