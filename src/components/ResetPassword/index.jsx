import React, { Component, Fragment } from 'react';

import './style.sass';

export default class ResetPassword extends Component {
	email = React.createRef();

	handleSubmit(ev) {
		ev.preventDefault();
		this.props.resetPassword(this.email.current.value);
	}

  render() {
    return (
      <section id="reset-password">
				<div className="popup">
					<div className="title">
						<h3>Сбросить пароль</h3>
					</div>
					<form onSubmit={ev => this.handleSubmit(ev)}>
						<label>
							<span>Введите Ваш email:</span>
							<input ref={this.email} type="email" name="email" placeholder="your_email@example.com" required />
						</label>
						<button>Сбросить пароль</button>
					</form>
				</div>
			</section>
    )
  }
}