import React from 'react';
import './style.sass';

export default () => (
  <span className="spinner-load">↻</span>
)