import React, { Component } from 'react';

import './style.sass';

export default class Footer extends Component {
  render() {
    return (
      <footer>
				<nav>
					<ul>
						<li><a href="#">Информация</a></li>
						<li><a href="#">Правила</a></li>
						<li><a href="#">Приватность</a></li>
						<li><a href="#">Отзыв</a></li>
						<li><a href="#">Цены</a></li>
					</ul>
				</nav>
				<div className="right-side">
					<div className="copy">
						<span>&copy; 2017 EAZZYCUT ltd. Все права защищены.</span>
					</div>
					<div className="social">
						<ul>
							<li><a href="#"><i className="fa fa-twitter-square"></i></a></li>
							<li><a href="#"><i className="fa fa-instagram"></i></a></li>
							<li><a href="#"><i className="fa fa-facebook-square"></i></a></li>
							<li><a href="#"><i className="fa fa-google-plus-square"></i></a></li>
							<li><a href="#"><i className="fa fa-linkedin-square"></i></a></li>
							<li><a href="#"><i className="fa fa-youtube-square"></i></a></li>
						</ul>
					</div>
				</div>
			</footer>
    )
  }
}