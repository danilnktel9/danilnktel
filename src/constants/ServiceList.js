import Film from '../../resources/svg/film.svg';
import Photobook from '../../resources/svg/photobook.svg';
import Retouch from '../../resources/svg/retouch-basic.svg';
import Collage from '../../resources/svg/collage.svg';

export default [
	{
		title: 'film',
		data: Film
	},
	{
		title: 'photobook',
		data: Photobook
	},
	{
		title: 'retouch',
		data: Retouch
	},
	{
		title: 'collage',
		data: Collage
	}
];