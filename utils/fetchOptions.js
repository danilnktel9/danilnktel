export default {
	method: "POST",
	mode: "cors",
	credentials: "include",
	headers: new Headers({
			"Content-Type": "application/json",
			"Accept": "application/json"
	})
};